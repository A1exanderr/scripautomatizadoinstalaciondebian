#!/bin/sh

# Función para pedir entrada de usuario con un valor por defecto
pedir_valor() {
  read -p "$1 [$2]: " valor
  echo "${valor:-$2}"
}

# Preguntar al usuario por la interfaz, la IP, la máscara de red y el gateway
interfaz=$(pedir_valor "Ingrese la interfaz de red a modificar" "enp1s0")
ip=$(pedir_valor "Ingrese la dirección IP estática" "192.168.1.49")
mascara=$(pedir_valor "Ingrese la máscara de red" "255.255.255.0")
network=$(echo "$ip" | awk -F"." '{print $1"."$2"."$3".0"}')
broadcast=$(echo "$ip" | awk -F"." '{print $1"."$2"."$3".255"}')
gateway_default=$(echo "$ip" | awk -F"." '{print $1"."$2"."$3".1"}')

# Preguntar si el gateway será el predeterminado o si quiere cambiarlo
read -p "El gateway predeterminado será $gateway_default. ¿Desea cambiarlo? (s/n): " respuesta

if [ "$respuesta" = "s" ]; then
  gateway=$(pedir_valor "Ingrese el gateway" "$gateway_default")
else
  gateway="$gateway_default"
fi

# Vamos a configurar la IP estática
ruta_red='/etc/network/interfaces'

# Eliminar y crear el archivo de configuración de red
rm -f "$ruta_red"
touch "$ruta_red"

# Escribir la configuración de la interfaz de red
cat <<EOT >> "$ruta_red"
source /etc/network/interfaces.d/*

auto lo
iface lo inet loopback

auto $interfaz
iface $interfaz inet static
    address $ip
    netmask $mascara
    network $network
    broadcast $broadcast
    gateway $gateway
EOT

# Mensaje de confirmación
echo '=============================================='
echo -e "\tSe cambió correctamente la IP de la interfaz $interfaz"
echo -e "\n\tNueva configuración:"
echo -e "\tIP: $ip"
echo -e "\tMáscara de red: $mascara"
echo -e "\tNetwork: $network"
echo -e "\tBroadcast: $broadcast"
echo -e "\tGateway: $gateway"
echo '=============================================='

# Preguntar si se debe reiniciar
read -p "¿Desea reiniciar el sistema ahora? (s/n): " reiniciar

if [ "$reiniciar" = "s" ]; then
  reboot
else
  echo "Reinicio cancelado. No olvide reiniciar el sistema para aplicar los cambios."
fi
