#!/bin/sh

#VAMOS A CONFIGURAR LA IP STATICA
ruta_red='/etc/network/interfaces';
rm $ruta_red;
touch $ruta_red;

echo 'source /etc/network/interfaces.d/*' >> $ruta_red;
echo '\n';
echo 'auto lo' >> $ruta_red;
echo 'iface lo inet loopback' >> $ruta_red;
echo '\n';
echo 'auto enp0s3' >> $ruta_red;
echo 'iface enp0s3 inet static' >> $ruta_red;
echo 'address 172.0.0.199' >> $ruta_red;
echo 'netmask 255.255.255.0' >> $ruta_red;
echo 'network 172.0.0.0' >> $ruta_red;
echo 'broadcast 172.0.0.255' >> $ruta_red;
echo 'gateway 172.0.0.1' >> $ruta_red;

echo '==============================================';
echo '\tSE CAMBIO CORECTAMENTE LA IP DEL SERVIDOR';
echo '\n a 172.0.0.199';
echo '==============================================';
reboot
