#!/bin/sh

#VAMOS A CONFIGURAR LA IP STATICA
ruta_red='/etc/network/interfaces';
rm $ruta_red;
touch $ruta_red;

echo 'source /etc/network/interfaces.d/*' >> $ruta_red;
echo '\n';
echo 'auto lo' >> $ruta_red;
echo 'iface lo inet loopback' >> $ruta_red;
echo '\n';
echo 'auto enp0s3' >> $ruta_red;
echo 'iface enp0s3 inet static' >> $ruta_red;
echo 'address 192.168.1.11' >> $ruta_red;
echo 'netmask 255.255.255.0' >> $ruta_red;
echo 'network 192.168.1.0' >> $ruta_red;
echo 'broadcast 192.168.1.255' >> $ruta_red;
echo 'gateway 192.168.1.1' >> $ruta_red;

echo '==============================================';
echo '\tSE CAMBIO CORECTAMENTE LA IP DEL SERVIDOR';
echo '\n a 192.168.1.11';
echo '==============================================';
reboot
