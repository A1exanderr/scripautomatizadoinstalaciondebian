#!/bin/sh

#BORAREMOS EL REPOSITORIO POR DEFAUL Y CREAREMOS UNO NUEVO
echo "========================================================="
if [[ $EUID -ne 0 ]]; then
   echo "EL SCRIPT TIENE QUE SER EJECUTADO CON EL USUARIO ROOT" 1>&2
   exit 1
fi
echo "========================================================="
echo "Por favor ingrese su nombre de usuario"
read name_user
echo "========================================================="
rm /etc/apt/sources.list
touch /etc/apt/sources.list

echo 'deb http://deb.debian.org/debian bullseye main' >> /etc/apt/sources.list
echo 'deb-src http://deb.debian.org/debian bullseye main' >> /etc/apt/sources.list
echo 'deb http://security.debian.org/debian-security bullseye-security main' >> /etc/apt/sources.list
echo 'deb-src http://security.debian.org/debian-security bullseye-security main' >> /etc/apt/sources.list
echo 'deb http://deb.debian.org/debian bullseye-updates main' >> /etc/apt/sources.list
echo 'deb-src http://deb.debian.org/debian bullseye-updates main' >> /etc/apt/sources.list
echo 'deb http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list
echo 'deb-src http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list
echo '' >> /etc/apt/sources.list
echo 'deb http://ftp.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list

#AHORA VAMOS A ACTUALIZAR LOS REPOSITORIOS
apt-get update
apt-get upgrade

sudo apt-get install figlet
figlet ITICORP | sudo tee /etc/motd

timedatectl set-timezone America/La_Paz
#timedatectl

echo 'INSTALACION DE VSFTPD';
apt-get install vsftpd ftp

rm /etc/vsftpd.conf
touch /etc/vsftpd.conf

echo 'AGREGAREMOS LAS CONFIGURACIONES NECESARIAS';

echo 'listen=YES' >> /etc/vsftpd.conf
echo 'listen_ipv6=NO' >> /etc/vsftpd.conf
echo 'anonymous_enable=NO' >> /etc/vsftpd.conf
echo 'local_enable=YES' >> /etc/vsftpd.conf
echo 'write_enable=YES' >> /etc/vsftpd.conf
echo 'dirmessage_enable=YES' >> /etc/vsftpd.conf
echo 'use_localtime=YES' >> /etc/vsftpd.conf
echo 'xferlog_enable=YES' >> /etc/vsftpd.conf
##echo 'xferlog_enable=YES' >> /etc/vsftpd.conf
echo 'connect_from_port_20=YES' >> /etc/vsftpd.conf
echo 'ascii_upload_enable=YES' >> /etc/vsftpd.conf
echo 'ascii_download_enable=YES' >> /etc/vsftpd.conf
echo 'chroot_local_user=YES' >> /etc/vsftpd.conf
echo 'chroot_list_enable=YES' >> /etc/vsftpd.conf
echo 'chroot_list_file=/etc/vsftpd.chroot_list' >> /etc/vsftpd.conf
echo 'ls_recurse_enable=YES' >> /etc/vsftpd.conf
echo 'secure_chroot_dir=/var/run/vsftpd/empty' >> /etc/vsftpd.conf
echo 'pam_service_name=vsftpd' >> /etc/vsftpd.conf
echo 'rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem' >> /etc/vsftpd.conf
echo 'rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key' >> /etc/vsftpd.conf
echo 'ssl_enable=NO' >> /etc/vsftpd.conf
echo '' >> /etc/vsftpd.conf

echo 'CONFIGURACION TERMINADA';
echo "VAMOS A CREAR UN USUARIO $name_user para ftp en la ruta /var/www/$name_user";
mkdir -p /var/www/
mkdir /var/www/$name_user
adduser --force-badname -home /var/www/$name_user/ -no-create-home -shell /bin/bash $name_user

#Tiempo de espera para la ejecucion del siguiente linea de comando
#sleep 60

echo "VAMOS A AGREGAR AL USUARIO $name_user en grupo ftp";
addgroup $name_user ftp
chown $name_user:root /var/www/$name_user/ -R

touch /etc/vsftpd.chroot_list
echo "$name_user" >> /etc/vsftpd.chroot_list

echo 'VAMOS A REINICIAR TU SERVIDOR FTP';

/etc/init.d/vsftpd restart

echo 'Vamos a configurar configuracion basica para el usuario';
bashrc_ruta="/var/www/$name_user/.bashrc";
touch $bashrc_ruta
cat << 'EOF' > "/var/www/$name_user/.bashrc"

case $- in
    *i*) ;;
      *) return;;
esac
HISTCONTROL=ignoreboth
shopt -s histappend
HISTSIZE=1000
HISTFILESIZE=2000

shopt -s checkwinsize

if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

EOF


echo 'CONFIGURAREMOS EL ARCHIVO PROFILE';
#Vamos a crear una varia con la siguiente ruta
profile_ruta="/var/www/$name_user/.profile";

touch $profile_ruta

echo -e 'if [ -n "$BASH_VERSION" ]; then' >> $profile_ruta
echo -e '\tif [ -f "$HOME/.bashrc" ]; then' >> $profile_ruta
echo -e '\t. "$HOME/.bashrc"' >> $profile_ruta
echo -e '\tfi' >> $profile_ruta
echo -e 'fi' >> $profile_ruta
echo '' >> $profile_ruta
echo -e 'if [ -d "$HOME/bin" ] ; then' >> $profile_ruta
echo -e '\tPATH="$HOME/bin:$PATH"' >> $profile_ruta
echo -e 'fi' >> $profile_ruta
echo -e '' >> $profile_ruta
echo -e 'if [ -d "$HOME/.local/bin" ] ; then' >> $profile_ruta
echo -e '\tPATH="$HOME/.local/bin:$PATH"' >> $profile_ruta
echo -e 'fi' >> $profile_ruta

echo 'VAMOS A CONFIGURAR EL ERROR DE DEBIAN';
dpkg --configure -a
export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin
echo 'export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin' >> $bashrc_ruta

echo 'Vamos a configurar usuario con sudo';
apt-get install sudo
whoami
usermod -aG sudo $name_user
#reboot
chown $name_user:root $bashrc_ruta
chown $name_user:root $profile_ruta

ruta_developer="/var/www/$name_user"
cp -r .instalacion_framework $ruta_developer
chown $name_user:root $ruta_developer/.instalacion_framework/ -R
chown $name_user:root $ruta_developer/.instalacion_framework/*

echo '==============================================';
echo -e '\tFIN DE LA INSTALACION';
echo '==============================================';
