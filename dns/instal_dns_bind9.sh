#!/bin/bash
#VAMOS A INSTALAR BIND 9
sudo apt install curl
echo "========================================================="
if [[ $EUID -ne 0 ]]; then
   echo "EL SCRIPT TIENE QUE SER EJECUTADO CON EL USUARIO ROOT" 1>&2
   exit 1
fi
echo "========================================================="
echo "Por favor ingrese su DNS que quiere crearlo ejemplo: fenix.com"
read dns
ip_publica=$(curl -s ifconfig.me)

# Verificar si curl tuvo éxito
if [ -z "$ip_publica" ]; then
	echo "Por favor ingrese su ip puede ser la publica o la privada por ejemplo 192.168.1.254"
else
	echo "Por favor ingrese su ip puede ser la publica o la privada por ejemplo Tu IP pública es: $ip_publica"
fi
read ip
ip_inversa=$(echo $ip | awk -F'.' '{print $3"."$2"."$1}')
host_ns="ns1"
echo "==================================="
echo "Tu dns es: $dns"
echo "Tu ip es: $ip"
echo "Ip inversa es: $ip_inversa"
echo "FQDN va ser: $host_ns.$dns"
echo "==================================="
continuar() {
	echo "Ejecutando la siguiente parte del script..."
	# Coloca aquí el código que deseas ejecutar al continuar
	sudo apt-get install bind9 bind9utils bind9-doc -y
	archivo_1="/etc/bind/named.conf.options"
	sudo rm -f $archivo_1
	sudo touch $archivo_1
	echo 'options {' >> $archivo_1
	echo -e '\tdirectory "/var/cache/bind";' >> $archivo_1
	echo '' >> $archivo_1
	echo -e '\tlisten-on { any; };' >> $archivo_1
	echo -e '\tallow-query { any; };' >> $archivo_1
	echo -e '\tforwarders {' >> $archivo_1
	echo -e '\t\t8.8.8.8;' >> $archivo_1
	echo -e '\t\t8.8.4.4;' >> $archivo_1
	echo -e '\t};' >> $archivo_1
	echo -e '\tdnssec-validation no;' >> $archivo_1
	echo '};' >> $archivo_1

	echo 'PASAMOS AL SIGIENTE ARCHIVO 2'
	archivo_2="/etc/default/named"
	sudo rm -f $archivo_2
	sudo touch $archivo_2
	echo 'RESOLVCONF=no' >> $archivo_2
	echo'' >> $archivo_2
	echo 'OPTIONS="-u bind -4"' >> $archivo_2
	echo "VERIFICAMOS LAS CONFIGURACIONES"
	sudo named-checkconf
	sudo systemctl restart bind9
	archivo_3="/etc/bind/named.conf.local"
	sudo rm -f $archivo_3
	sudo touch $archivo_3
	echo 'zone "'$dns'" IN {' >> $archivo_3
	echo -e '\ttype master;' >> $archivo_3
	echo -e '\tfile "/etc/bind/zonas/db.directa";' >> $archivo_3
	echo '};' >> $archivo_3
	echo '' >> $archivo_3
	echo 'zone "'$ip_inversa'.in-addr.arpa" {' >> $archivo_3
	echo -e '\ttype master;' >> $archivo_3
	echo -e '\tfile "/etc/bind/zonas/db.inversa";' >> $archivo_3
	echo '};' >> $archivo_3
	sudo mkdir /etc/bind/zonas/
	archivo_4="/etc/bind/zonas/db.directa"
	sudo touch $archivo_4
	echo -e '$TTL\t1D' >> $archivo_4
	echo -e "@\tIN\tSOA\t$host_ns.$dns. admin.$dns. (" >> $archivo_4
	echo -e '\t1\t\t; Serial' >> $archivo_4
	echo -e '\t12h\t\t; Refresh' >> $archivo_4
	echo -e '\t15m\t\t; Retry' >> $archivo_4
	echo -e '\t3w\t\t; Expire' >> $archivo_4
	echo -e '\t2h )\t\t; Negative Cache TTL' >> $archivo_4
	echo -e ';\tRegistros NS' >> $archivo_4
	echo -e '' >> $archivo_4
	echo -e "\tIN\tNS\t$host_ns.$dns." >> $archivo_4
	echo -e "$host_ns\tIN\tA\t$ip" >> $archivo_4
	echo -e ";@\tIN\tNS\temail.$dns." >> $archivo_4
	echo -e ";@\tIN\tmx 10\temail.$dns." >> $archivo_4
	echo -e ";email\tIN\tA\t$ip" >> $archivo_4
	echo -e "www\tIN\tA\t$ip" >> $archivo_4

	archivo_5="/etc/bind/zonas/db.inversa"
	sudo touch $archivo_5
	echo -e '$TTL\t1d;' >> $archivo_5
	echo -e "@\tIN\tSOA\t$host_ns.$dns admin.$dns. (" >> $archivo_5
	echo -e '\t\t\t20210222\t\t; Serial' >> $archivo_5
        echo -e "\t\t\t12h\t\t; Refresh" >> $archivo_5
        echo -e '\t\t\t15m\t\t; Retry' >> $archivo_5
        echo -e '\t\t\t3w\t\t; Expire' >> $archivo_5
        echo -e '\t\t\t2h\t)\t; Negative Cache TTL' >> $archivo_5
	echo ';' >> $archivo_5
	ip_ultimo=$(echo $ip | awk -F'.' '{print $4}')
	echo -e "@\tIN\tNS\t$host_ns.$dns." >> $archivo_5
	echo -e "@\tIN\tNS\temail.$dns." >> $archivo_5
	echo -e "$ip_ultimo\tIN\tPTR\temail.$dns." >> $archivo_5
	echo -e "$ip_ultimo\tIN\tPTR\twww.$dns." >> $archivo_5
	
	sudo named-checkzone $dns /etc/bind/zonas/db.directa
	sudo named-checkzone $ip_inversa.in-addr.arpa /etc/bind/zonas/db.inversa
	sudo systemctl restart bind9
}

# Función que se ejecuta si el usuario presiona otra tecla
cancelar() {
	echo "Cancelando el script."
	exit 1
}

echo "Presiona Enter para continuar o cualquier otra tecla para cancelar."
read -r -n 1 -s key
if [[ $key == "" ]]; then
	continuar
else
	cancelar
fi
