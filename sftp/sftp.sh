#!/bin/bash

# Función para verificar si un paquete está instalado
function verificar_paquete() {
    paquete=$1
    if ! dpkg -l | grep -q $paquete; then
        echo "Instalando $paquete..."
        apt-get install -y $paquete
    else
        echo "$paquete ya está instalado"
    fi
}

# Verificación de permisos de usuario root
echo "========================================================="
if [[ $EUID -ne 0 ]]; then
   echo "EL SCRIPT TIENE QUE SER EJECUTADO CON EL USUARIO ROOT" 1>&2
   exit 1
fi
echo "========================================================="

# Verificación e instalación de los paquetes necesarios
verificar_paquete "figlet"
verificar_paquete "vsftpd"
verificar_paquete "ftp"
verificar_paquete "sudo"

# Mostrar banner
figlet ITICORP | sudo tee /etc/motd

# Establecer la zona horaria
timedatectl set-timezone America/La_Paz

# Crear y configurar el archivo de configuración de vsftpd
if [ ! -f /etc/vsftpd.conf ]; then
    echo "El archivo de configuración de vsftpd no existe. Creando nueva configuración..."
    cat <<EOL > /etc/vsftpd.conf
listen=YES
listen_ipv6=NO
anonymous_enable=NO
local_enable=YES
write_enable=YES
dirmessage_enable=YES
use_localtime=YES
xferlog_enable=YES
connect_from_port_20=YES
ascii_upload_enable=YES
ascii_download_enable=YES
chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd.chroot_list
ls_recurse_enable=YES
secure_chroot_dir=/var/run/vsftpd/empty
pam_service_name=vsftpd
rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
ssl_enable=NO
EOL
    echo 'Configuración de vsftpd creada correctamente.'
else
    echo "El archivo de configuración de vsftpd ya existe."
    echo "¿Deseas actualizarlo? (s/n)"
    read actualizar
    if [[ "$actualizar" == "s" || "$actualizar" == "S" ]]; then
        echo "Actualizando el archivo de configuración de vsftpd..."
        cat <<EOL > /etc/vsftpd.conf
listen=YES
listen_ipv6=NO
anonymous_enable=NO
local_enable=YES
write_enable=YES
dirmessage_enable=YES
use_localtime=YES
xferlog_enable=YES
connect_from_port_20=YES
ascii_upload_enable=YES
ascii_download_enable=YES
chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd.chroot_list
ls_recurse_enable=YES
secure_chroot_dir=/var/run/vsftpd/empty
pam_service_name=vsftpd
rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem
rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key
ssl_enable=NO
EOL
        echo 'Archivo de configuración de vsftpd actualizado correctamente.'
    else
        echo "No se ha actualizado el archivo de configuración."
    fi
fi

# Función para crear un usuario FTP
function crear_usuario_ftp() {
    echo "Por favor ingrese el nombre de usuario que desea crear"
    read name_user

    # Verificar si el usuario ya existe
    if id "$name_user" &>/dev/null; then
        echo "El usuario $name_user ya existe."
    else
        echo "Creando usuario $name_user para FTP..."
        
        # Crear el directorio del usuario y copiar los archivos de configuración predeterminados
        mkdir -p /var/www/$name_user
        
        # Crear el usuario
        sudo adduser --home /var/www/$name_user --shell /bin/bash --no-create-home $name_user
        
        # Copiar archivos de configuración predeterminados desde /etc/skel
        cp /etc/skel/.bashrc /var/www/$name_user/
        cp /etc/skel/.profile /var/www/$name_user/
        
        echo "Usuario $name_user creado con archivos de configuración predeterminados."

        # Ajustar permisos del directorio del usuario
        chown $name_user:root /var/www/$name_user/ -R

        # Añadir el usuario al archivo de chroot si no está ya agregado
        if grep -Fxq "$name_user" /etc/vsftpd.chroot_list; then
            echo "El usuario $name_user ya está en la lista de chroot"
        else
            echo "$name_user" >> /etc/vsftpd.chroot_list
            echo "Usuario $name_user agregado a la lista de chroot"
        fi
	echo "Agregamos al usuario con el privilegio sudo"
	sudo usermod -aG sudo $name_user
	echo 'VAMOS A CONFIGURAR EL ERROR DE DEBIAN';
	bashrc_ruta="/var/www/$name_user/.bashrc";
	#dpkg --configure -a
	export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin
	echo 'export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin' >> $bashrc_ruta
    fi
}
# Función para listar todos los usuarios del sistema
function listar_usuarios() {
    awk -F':' '{ if ($3 >= 1000 && $3 < 65534) print $1 }' /etc/passwd
}

# Función para agregar un usuario existente al grupo FTP
function agregar_usuario_grupo_ftp() {
    echo "Seleccione el usuario que desea agregar al grupo FTP:"
    
    # Listar todos los usuarios del sistema
    usuarios=( $(listar_usuarios) )
    
    # Mostrar una lista numerada de los usuarios
    select name_user in "${usuarios[@]}"; do
        if [[ -n "$name_user" ]]; then
            echo "Has seleccionado: $name_user"
            break
        else
            echo "Opción no válida. Intente de nuevo."
        fi
    done

    # Verificar si el usuario ya está en el grupo FTP
    if groups $name_user | grep &>/dev/null '\bftp\b'; then
        echo "El usuario $name_user ya pertenece al grupo FTP."
    else
        echo "Agregando al usuario $name_user al grupo FTP..."
        addgroup $name_user ftp
        echo "Usuario $name_user agregado al grupo FTP."
    fi
}

# Función para mostrar el menú
function mostrar_menu() {
    echo "========================================="
    echo "           MENÚ DE OPCIONES              "
    echo "========================================="
    echo "1. Crear un nuevo usuario FTP"
    echo "2. Agregar un usuario existente al grupo FTP"
    echo "3. Salir"
    echo "========================================="
    echo "Seleccione una opción:"
    read opcion

    case $opcion in
        1)
            crear_usuario_ftp
            ;;
        2)
            agregar_usuario_grupo_ftp
            ;;
        3)
            echo "Saliendo del script."
            exit 0
            ;;
        *)
            echo "Opción no válida. Por favor seleccione una opción válida."
            mostrar_menu
            ;;
    esac
}

# Mostrar el menú al usuario
mostrar_menu

# Reiniciar el servicio vsftpd
echo 'Reiniciando el servidor FTP...'
/etc/init.d/vsftpd restart

echo '=============================================='
echo -e '\tFIN DEL PROCESO'
echo '=============================================='
