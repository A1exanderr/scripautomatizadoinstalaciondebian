#!/bin/sh
echo '==================================';
echo 'INSTALACION DE ANGULAR';
echo '==================================';

sudo apt-get update
sudo apt-get upgrade

sudo apt-get install curl npm -y

echo 'PARA REVISAR EL CODIGO';
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh
echo 'EJECUTAMOS EL CODIGO';
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
echo 'Agregamos en entorno de variable';
source ~/.bashrc
echo 'Escogemos la version de node para la instalacion';
nvm list-remote

node_version='v18.14.0';
echo 'INSTALAMOS LA VERSION ELIGIDA';
nvm install $node_version
nvm list
#nvm install lts/erbium
nvm use $node_version
nvm use --delete-prefix $node_version
node -v
npm install -g npm@9.4.2
echo 'INSTALACION DE ANGULAR CLI';
npm install -g @angular/cli
ng help
#sudo apt-get install npm
echo '=====================================';
echo '\nFIN DE LA INSTALACION '
echo '====================================';
echo 'Reiniciando el S.O. o el Equipo';
sleep 15
sudo reboot
