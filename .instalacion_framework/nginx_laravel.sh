#!/bin/sh

echo 'INSTLACION DE LARAVEL CON NGINX';
sudo apt-get update
sudo apt-get upgrade

sudo apt-get install nginx php php-fpm php-mail php-mail-mime php-mysql php-gd php-common php-pear php-mbstring php-xml php-curl php-json php-zip unzip wget -y

sudo phpenmod mbstring

sudo systemctl restart nginx


echo 'VAMOS CON LA INSTALACION DE COMPOSER EN MODO GLOBAL....'

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"

ls
php -r "if (hash_file('sha384', 'composer-setup.php') === 'dac665fdc30fdd8ec78b38b9800061b4150413ff2e3b6f88543c636f7cd84f6db9189d43a81e5503cda447da73c7e5b6') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
echo 'Se ejecutara la instalacion de composer....';
php composer-setup.php
sudo mv composer.phar /usr/local/bin/composer
composer -v

rm composer-setup.php
echo 'Instalamos el laravel en modo global del usuario'

composer global require laravel/installer

echo $PATH
echo 'export PATH="$HOME/.config/composer/vendor/bin:$PATH"'
echo 'export PATH="$HOME/.config/composer/vendor/bin:$PATH"' >> ~/.bashrc
source ~/.bashrc

echo '=====================================================';
echo 'FIN DE LA INSTALACION';
echo '=====================================================';
echo 'Reiniciando el SO o el Equipo'
sleep 15
sudo reboot
