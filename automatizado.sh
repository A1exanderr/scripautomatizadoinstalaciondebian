#!/bin/sh

#BORAREMOS EL REPOSITORIO POR DEFAUL Y CREAREMOS UNO NUEVO
rm /etc/apt/sources.list
touch /etc/apt/sources.list

echo 'deb http://deb.debian.org/debian bullseye main' >> /etc/apt/sources.list
echo 'deb-src http://deb.debian.org/debian bullseye main' >> /etc/apt/sources.list
echo 'deb http://security.debian.org/debian-security bullseye-security main' >> /etc/apt/sources.list
echo 'deb-src http://security.debian.org/debian-security bullseye-security main' >> /etc/apt/sources.list
echo 'deb http://deb.debian.org/debian bullseye-updates main' >> /etc/apt/sources.list
echo 'deb-src http://deb.debian.org/debian bullseye-updates main' >> /etc/apt/sources.list
echo 'deb http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list
echo 'deb-src http://deb.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list
echo '' >> /etc/apt/sources.list
echo 'deb http://ftp.debian.org/debian bullseye-backports main' >> /etc/apt/sources.list

#AHORA VAMOS A ACTUALIZAR LOS REPOSITORIOS
apt-get update
apt-get upgrade

#timedatectl set-timezone America/La_Paz
#timedatectl
sudo apt-get install figlet
figlet ITICORP | sudo tee /etc/motd

echo 'INSTALACION DE VSFTPD';
apt-get install vsftpd ftp

rm /etc/vsftpd.conf
touch /etc/vsftpd.conf

echo 'AGREGAREMOS LAS CONFIGURACIONES NECESARIAS';

echo 'listen=YES' >> /etc/vsftpd.conf
echo 'listen_ipv6=NO' >> /etc/vsftpd.conf
echo 'anonymous_enable=NO' >> /etc/vsftpd.conf
echo 'local_enable=YES' >> /etc/vsftpd.conf
echo 'write_enable=YES' >> /etc/vsftpd.conf
echo 'dirmessage_enable=YES' >> /etc/vsftpd.conf
echo 'use_localtime=YES' >> /etc/vsftpd.conf
echo 'xferlog_enable=YES' >> /etc/vsftpd.conf
##echo 'xferlog_enable=YES' >> /etc/vsftpd.conf
echo 'connect_from_port_20=YES' >> /etc/vsftpd.conf
echo 'ascii_upload_enable=YES' >> /etc/vsftpd.conf
echo 'ascii_download_enable=YES' >> /etc/vsftpd.conf
echo 'chroot_local_user=YES' >> /etc/vsftpd.conf
echo 'chroot_list_enable=YES' >> /etc/vsftpd.conf
echo 'chroot_list_file=/etc/vsftpd.chroot_list' >> /etc/vsftpd.conf
echo 'ls_recurse_enable=YES' >> /etc/vsftpd.conf
echo 'secure_chroot_dir=/var/run/vsftpd/empty' >> /etc/vsftpd.conf
echo 'pam_service_name=vsftpd' >> /etc/vsftpd.conf
echo 'rsa_cert_file=/etc/ssl/certs/ssl-cert-snakeoil.pem' >> /etc/vsftpd.conf
echo 'rsa_private_key_file=/etc/ssl/private/ssl-cert-snakeoil.key' >> /etc/vsftpd.conf
echo 'ssl_enable=NO' >> /etc/vsftpd.conf
echo '' >> /etc/vsftpd.conf
echo 'listen_port=2023' >> /etc/vsftpd.conf

echo 'CONFIGURACION TERMINADA';
echo 'VAMOS A CREAR UN USUARIO developer para ftp en la ruta /var/www/developer';

mkdir /var/www/developer
adduser --force-badname -home /var/www/developer/ -no-create-home -shell /bin/bash developer

#Tiempo de espera para la ejecucion del siguiente linea de comando
#sleep 60

echo 'VAMOS A AGREGAR AL USUARIO developer en grupo ftp';
addgroup developer ftp
chown developer:root /var/www/developer/ -R

touch /etc/vsftpd.chroot_list
echo 'developer' >> /etc/vsftpd.chroot_list

echo 'VAMOS A REINICIAR TU SERVIDOR FTP';

/etc/init.d/vsftpd restart

echo 'Vamos a configurar configuracion basica para el usuario';

bashrc_ruta='/var/www/developer/.bashrc';

touch $bashrc_ruta

echo 'case $- in' >> $bashrc_ruta
echo '\t*i*) ;;' >> $bashrc_ruta
echo '\t\t*) return;;' >> $bashrc_ruta
echo 'esac' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'HISTCONTROL=ignoreboth' >> $bashrc_ruta
echo 'shopt -s histappend' >> $bashrc_ruta
echo 'HISTSIZE=1000' >> $bashrc_ruta
echo 'HISTFILESIZE=2000' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'shopt -s checkwinsize' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then' >> $bashrc_ruta
echo '\tdebian_chroot=$(cat /etc/debian_chroot)' >> $bashrc_ruta
echo 'fi' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'case "$TERM" in' >> $bashrc_ruta
echo '\txterm-color|*-256color) color_prompt=yes;;' >> $bashrc_ruta
echo 'esac' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'if [ -n "$force_color_prompt" ]; then' >> $bashrc_ruta
echo '\tif [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then' >> $bashrc_ruta
echo '\tcolor_prompt=yes' >> $bashrc_ruta
echo '\telse' >> $bashrc_ruta
echo '\tcolor_prompt=' >> $bashrc_ruta
echo '\tfi' >> $bashrc_ruta
echo 'fi' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'if [ "$color_prompt" = yes ]; then' >> $bashrc_ruta
echo "\tPS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '" >> $bashrc_ruta
echo 'else' >> $bashrc_ruta
echo "\tPS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '" >> $bashrc_ruta
echo 'fi' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'unset color_prompt force_color_prompt' >> $bashrc_ruta
echo 'case "$TERM" in' >> $bashrc_ruta
echo 'xterm*|rxvt*)' >> $bashrc_ruta
echo '\tPS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"' >> $bashrc_ruta
echo '\t;;' >> $bashrc_ruta
echo '*)' >> $bashrc_ruta
echo '\t;;' >> $bashrc_ruta
echo 'esac' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'if [ -x /usr/bin/dircolors ]; then' >> $bashrc_ruta
echo '\ttest -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"' >> $bashrc_ruta
echo "\talias ls='ls --color=auto'" >> $bashrc_ruta
echo 'fi' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'if [ -f ~/.bash_aliases ]; then' >> $bashrc_ruta
echo '\t. ~/.bash_aliases' >> $bashrc_ruta
echo 'fi' >> $bashrc_ruta
echo '' >> $bashrc_ruta
echo 'if ! shopt -oq posix; then' >> $bashrc_ruta
echo '\tif [ -f /usr/share/bash-completion/bash_completion ]; then' >> $bashrc_ruta
echo '\t\t. /usr/share/bash-completion/bash_completion' >> $bashrc_ruta
echo '\telif [ -f /etc/bash_completion ]; then' >> $bashrc_ruta
echo '\t\t. /etc/bash_completion' >> $bashrc_ruta
echo '\tfi' >> $bashrc_ruta
echo 'fi' >> $bashrc_ruta
echo '' >> $bashrc_ruta

echo 'CONFIGURAREMOS EL ARCHIVO PROFILE';
#Vamos a crear una varia con la siguiente ruta
profile_ruta='/var/www/developer/.profile';

touch $profile_ruta

echo 'if [ -n "$BASH_VERSION" ]; then' >> $profile_ruta
echo '\tif [ -f "$HOME/.bashrc" ]; then' >> $profile_ruta
echo '\t. "$HOME/.bashrc"' >> $profile_ruta
echo '\tfi' >> $profile_ruta
echo 'fi' >> $profile_ruta
echo '' >> $profile_ruta
echo 'if [ -d "$HOME/bin" ] ; then' >> $profile_ruta
echo '\tPATH="$HOME/bin:$PATH"' >> $profile_ruta
echo 'fi' >> $profile_ruta
echo '' >> $profile_ruta
echo 'if [ -d "$HOME/.local/bin" ] ; then' >> $profile_ruta
echo '\tPATH="$HOME/.local/bin:$PATH"' >> $profile_ruta
echo 'fi' >> $profile_ruta

echo 'VAMOS A CONFIGURAR EL ERROR DE DEBIAN';
dpkg --configure -a
export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin
echo 'export PATH=$PATH:/usr/local/sbin:/usr/sbin:/sbin' >> $bashrc_ruta

echo 'Vamos a configurar usuario con sudo';
apt-get install sudo
whoami
usermod -aG sudo developer
#reboot
chown developer:root $bashrc_ruta
chown developer:root $profile_ruta

ruta_developer='/var/www/developer'
cp -r .instalacion_framework $ruta_developer
chown developer:root $ruta_developer/.instalacion_framework/ -R
chown developer:root $ruta_developer/.instalacion_framework/*

echo '==============================================';
echo '\tFIN DE LA INSTALACION';
echo '==============================================';
