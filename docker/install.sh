#!/bin/sh

#VAMOS A INSTAKAR EL DOCKER
sudo apt update
sudo apt upgrade
#Instalamos los paquetes necesarios
sudo apt install apt-transport-https ca-certificates curl software-properties-common
#Agregamos la clave oficial de Docker
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
#Agregamos el repositorio de docker
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#Actulizamos el repositorio
sudo apt update
#Instamos el docker con el siguiente comando
sudo apt install docker-ce
#Verificamos si esta instaldo corectamente
sudo systemctl status docker
#Añadimos al usuario al grupo docker
sudo usermod -aG docker $USER
#=============================================================
#=============================================================
#instalamos el portainer para controlar desde la web
#primero crearemos un volumen para que se almacen los datos
sudo docker volume create portainer_data
#con el siguiente comando ejecutamos se descarga la imagen y se instala
sudo docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce
#Abre tu navegador y ve a http://<tu-IP-o-localhost>:9000
#ver si esta coriendo
sudo docker ps
