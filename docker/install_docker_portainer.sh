#!/bin/bash

# Verifica si el script se está ejecutando como root
if [ "$(id -u)" != "0" ]; then
   echo "Este script debe ejecutarse como root o con sudo" 
   exit 1
fi

echo "Actualizando el sistema..."
apt update && apt upgrade -y

echo "Instalando paquetes necesarios..."
apt install -y apt-transport-https ca-certificates curl software-properties-common

echo "Agregando la clave GPG oficial de Docker..."
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "Agregando el repositorio de Docker..."
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

echo "Actualizando la lista de paquetes..."
apt update

echo "Instalando Docker..."
apt install -y docker-ce docker-ce-cli containerd.io

echo "Iniciando y habilitando el servicio de Docker..."
systemctl start docker
systemctl enable docker

echo "Agregando al usuario actual al grupo docker..."
usermod -aG docker $SUDO_USER

echo "Instalación de Docker completa. Recuerda cerrar y volver a abrir la sesión para aplicar los cambios de grupo."

# Preguntar si se desea instalar Portainer
read -p "¿Deseas instalar Portainer para administrar Docker desde la web? (s/n): " instalar_portainer

if [[ "$instalar_portainer" =~ ^[sS]$ ]]; then
    echo "Creando volumen para Portainer..."
    docker volume create portainer_data

    echo "Descargando y ejecutando Portainer..."
    docker run -d -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce

    echo "Portainer ha sido instalado y está disponible en http://<tu-IP-o-localhost>:9000"
    echo "Por favor, configura un usuario y una contraseña en la interfaz web."
else
    echo "No se instalará Portainer."
fi
